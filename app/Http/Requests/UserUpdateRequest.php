<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:255|string',
            'email' => 'required|email',
        ];
    }

    public function messages()
    {
        return[
            'name.required' => 'Ad soyad boş bırakılamaz.',
            'name.min' => 'Ad soyad en az 5 karakterden oluşmalıdır.',
            'name.max' => 'Ad soyad en fazla 255 karakterden oluşabilir.',
            'name.string' => 'Ad soyad sayısal karakter içermemelidir.',
            'email.required' => 'E-posta adresi boş bırakılamaz.',
            'email.email' => 'Lütfen geçerli bir e-posta adresi girin',
        ];
    }
}
