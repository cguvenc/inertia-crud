<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        return Inertia::render('Dashboard');
    }

    public function users(Request $request)
    {
        return Inertia::render('Users/Index', [
            'users' => User::query()->when(
                $request->input('search'),
                function ($query, $search) {
                    $query->where('name', 'like', '%' . $search . '%');
                }
            )
                ->paginate(10)
                ->withQueryString()
                ->through(fn ($user) => [
                    'email' => $user->email,
                    'name' => $user->name,
                    'id' => $user->id
                ]),
            'target' => $request->input('search')
        ]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return Inertia::render('Users/Edit', [
            'user' => [
                'name' => $user->name,
                'email' => $user->email,
                'id' => $id
            ]
        ]);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::find($id);

        $request->validate([
            'email' => 'unique:users,email,'.$id.',id'
        ],
        [
            'email.unique' => 'Bu e-posta adresi başka bir hesap tarafından kullanılıyor.',
        ]);
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect()->route('Users');
    }

    public function add()
    {
        return Inertia::render('Users/Add');
    }

    public function addPost(UserCreateRequest $request)
    {
        User::create($request->post());
        return redirect()->route('Users');
    }

    public function delete($id)
    {
        User::find($id)->delete();
    }
}
