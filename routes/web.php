<?php

use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});


Route::middleware(['auth', 'verified'])->group(function () {
    
    Route::get('/dashboard',[UserController::class,'index'])->name('dashboard');
    Route::get('/users',[UserController::class,'users'])->name('Users');
    Route::get('user/{id}',[UserController::class,'edit'])->name('edit');
    Route::put('user/{id}',[UserController::class,'update'])->name('update');
    Route::get('/add',[UserController::class,'add'])->name('add');
    Route::post('/add', [UserController::class,'addPost'])->name('add-post');
    Route::delete('users/{id}',[UserController::class,'delete'])->name('delete');

});

require __DIR__ . '/auth.php';
